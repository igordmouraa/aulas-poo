/*Crie um array com 3 nomes de frutas.
Em seguida, use um loop while para iterar sobre o array e exibir cada fruta em uma linha separada.*/

namespace exercicioArray2
{
    let frutas: string[] = ['goiaba', 'mamao', 'tomate'];

    console.log(frutas[0]); //goiaba
    console.log(frutas[1]); //mamao
    console.log(frutas[2]); //tomate

    let j: number = 0;

    while (j < frutas.length)

    {
        console.log(frutas[j]);
        j++;
    }
    
}