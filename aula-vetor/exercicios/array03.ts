/*Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor. 
Em seguida, use o método map() para criar um novo array contendo apenas os títulos dos livros.*/

namespace exercicioArray3
{
    let livros: any[] = [

        { titulo: "o senhor dos aneis", autor: "marcos edson" }, 
        { titulo: "Harry Potter", autor:"João King" }, 
        { titulo: "Dora, Aventureira", autor: "Stan Lee" },
        { titulo: "Avenger: Endgame", autor:"steven ditko" },
        { titulo: 'O Senhor dos Anéis', autor: 'Autor 3' },
        { titulo: '1984', autor: 'Autor 3' },
        { titulo: 'A Revolução dos Bichos', autor: 'George Orwell' },
        { titulo: 'Cem Anos de Solidão', autor: 'Autor 3' },

    ];

    let titulos = livros.map((livro) => 
    {
        return livro.titulo
    
    });

    let autores = livros.map((livro) => 
    {
        return livro.autor

    });

    console.log(titulos);
    console.log(autores);

    let livrosAutor3 = livros.filter((livro) => 
    {
        return livro.autor === "Autor 3"

    });

    console.log(livrosAutor3);

    let titulosAutor3 = livrosAutor3.map((livros) =>
    {
        return livros.titulo
    });

    console.log(titulosAutor3);

}