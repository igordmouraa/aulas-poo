/*Crie um array com 5 números.
Em seguida, use um loop for para iterar sobre o array e exibir a soma de todos os números.*/ 

namespace exercicioArray
{                       //0, 1, 2, 3, 4, 5
    let numeros: number[] = [1, 2, 3, 4, 5];

    let somaNumeros: number=0;


    for (let i = 0; i < numeros.length; i++) {
        somaNumeros += numeros[i]; 

    }
    console.log(`A soma dos números é: ${somaNumeros}`);
};

/*Criando uma iteração com multiplicação
let multi: number = 1;

for (let i = 0; i <numeros.length; i++) {
    multi *= numeros'[0];

}

    console.log (`O resultado da multiplicação é: ${multi}`);*/

