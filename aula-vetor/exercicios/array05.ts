/*Crie um array vazio. Em seguida, use o método push() para adicionar 3 números ao array. 
Em seguida, use o método pop() para remover o último número do array e exibir o array resultante.*/

namespace exercicioArray5
{
    /* let numeros: number[] = [];
    let i = 0

    while ( i < 1000 ) {
        numeros.push( i + 1 );
        i++

    }

    console.log(numeros); */

    let numeros: number[] = [];

    numeros.push(1);
    numeros.push(2);
    numeros.push(3);
    
    numeros.pop();
    
    console.log(numeros); // [1, 2]

}