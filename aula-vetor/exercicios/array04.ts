/*Crie um array com 6 números.
Em seguida, use o método filter() para criar um novo array contendo apenas os números ímpares.*/

namespace exercicioArray4 
{
    let numeros: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 8, 10, 11, 12, 13, 14, 15];

    let impares = numeros.filter((number) => 
    {
        return number % 2 === 1;
    });

    console.log(impares);

}