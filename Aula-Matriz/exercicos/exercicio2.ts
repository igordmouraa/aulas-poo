namespace Marcos 
{
    const matriz: number[][] = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
    ];

    matriz.forEach((linha: number[]) => 
    {
        const soma = linha.reduce((acc: number, valor: number) => acc + valor, 0);
        const media = soma / linha.length;
        console.log(`A média da linha [${linha.join(', ')}] é ${media}`);
    });

}