namespace aulaFunction2 
{
    function multiplicarPor(n:number): (x:number) => number {
        return function (x:number): number {
            return x * n;
        }
    }

    let duplicar = multiplicarPor(2);
    let resultado = duplicar(6);
    console.log(`O valor da multiplicação é${resultado}`);

    let triplicar = multiplicarPor(3);
    resultado = triplicar(6)
    console.log(`O valor da multiplicação é ${resultado}`);
    
    
}